public class Fraction {
    private int nominator;
    private int dominator;

    public void setNominator(int nominator) {
        this.nominator = nominator;
    }

    public void setDominator(int dominator) {
        this.dominator = dominator;
    }

    public int getNominator() {
        return nominator;
    }

    public int getDominator() {
        return dominator;
    }

    public Fraction multiplyFraction(Fraction fraction) {
        int newNominator = this.nominator * fraction.getNominator();
        int newDominator = this.dominator * fraction.getDominator();
        return new Fraction(newNominator, newDominator);
    }

    public Fraction multiplyFraction(int number) {
        int newNominator = this.nominator * number;
        return new Fraction(newNominator, this.dominator);
    }

    public Fraction(int nominator, int dominator) {
        this.nominator = nominator;
        this.dominator = dominator;
        correction();
    }

    public Fraction copier() {
        return new Fraction(this.nominator, this.dominator);
    }

    public Fraction() {
        this.nominator = 0;
        this.dominator = 1;
    }

    public Fraction(int nominator) {
        this.nominator = nominator;
        this.dominator = 1;
    }

    public void reduce() {
        this.nominator = nominator / biggestDivider();
        this.dominator = dominator / biggestDivider();
    }

    private int biggestDivider() {
        int biggestDivider = 1;
        for (int i = dominator; i > 0; i--) {
            if ((dominator % i == 0) && (nominator % i == 0)) {
                biggestDivider = i;
            }
        }
        return biggestDivider;
    }

    private void correction() {
        if (dominator * nominator < 0) {
            nominator = Math.abs(nominator) * -1;
            dominator = Math.abs(dominator);
        } else {
            nominator = Math.abs(nominator);
            dominator = Math.abs(dominator);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static Fraction newFraction(Fraction firstFraction, Fraction secondFraction) {
        return new Fraction(firstFraction.getNominator() * secondFraction.getNominator(),
                firstFraction.getDominator() * secondFraction.getDominator());
    }

    public static Fraction newFraction(Fraction firstFraction, int number) {
        return new Fraction(firstFraction.getNominator() * number, firstFraction.getDominator());
    }
    public static Fraction newFraction(int number1, int number2){
        return new Fraction(number1 ,number2);
    }

}
